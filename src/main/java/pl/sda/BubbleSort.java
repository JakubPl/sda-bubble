package pl.sda;

import java.math.BigDecimal;
import java.util.Scanner;

public class BubbleSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String stringNumbers = scanner.nextLine();
        //10 20 30 40 0 1 5
        final String[] arrayOfNumbers = stringNumbers.split(" ");
        BigDecimal[] numbers = new BigDecimal[arrayOfNumbers.length];

        for (int i = 0; i < arrayOfNumbers.length; i++) {
            numbers[i] = new BigDecimal(arrayOfNumbers[i]);
        }

/*        List<Long> arrayOfLongs = new ArrayList<>();
        for (int i = 0; i < arrayOfNumbers.length; i ++) {
            arrayOfLongs.add(Long.valueOf(arrayOfNumbers[i]));
        }


        Arrays.stream(arrayOfNumbers)
                .map(e -> new BigDecimal(e))
                .toArray();*/
        sort(numbers);
        System.out.println("Wynik to:");
        printArray(numbers);
    }

    private static void printArray(BigDecimal[] arrayToPrint) {
        for (int i = 0; i < arrayToPrint.length; i++) {
            System.out.println(arrayToPrint[i]);
        }
    }

    private static void sort(BigDecimal[] numbers) {
        int n = numbers.length;
        boolean swapped = false;
        int numberOfComprehensions = 0;
        do {
            for (int i = 0; i < n - 1; i++) {
                numberOfComprehensions++;
                if (numbers[i].compareTo(numbers[i + 1]) > 0) {
                    swapped = true;
                    BigDecimal tmp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = tmp;
                }
            }
            if (!swapped) {
                break;
            }
            swapped = false;
            System.out.println();
            String messageOfIteration = new StringBuilder()
                    .append("Tablica w iteracji ")
                    .append(n)
                    .append(":")
                    .toString();
            System.out.println(messageOfIteration);
            printArray(numbers);
            paintChart(numbers);
            n--;
        } while (n > 1);
        System.out.println("Ilosc porownan");
        System.out.println(numberOfComprehensions);
    }

    private static void paintChart(BigDecimal[] arrayToPrint) {
        for (BigDecimal printingElement : arrayToPrint) {
            System.out.printf("%s: ", printingElement);
            for (BigDecimal i = BigDecimal.ZERO; i.compareTo(printingElement) < 0; i = i.add(BigDecimal.ONE)) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
